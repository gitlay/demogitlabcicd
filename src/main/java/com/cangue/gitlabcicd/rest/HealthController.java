package com.cangue.gitlabcicd.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = HealthController.BASE_URL)
public class HealthController {
    public static final String BASE_URL = "/api/v1";
    @GetMapping("/health")
    public String getMethodName() {
        return new String("Health Check end point is Running");
    }
}
